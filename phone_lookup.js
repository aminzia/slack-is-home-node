const exec = require('child_process').exec;
const respondSlack = require('./respond-slack');
const missingMessage = "Naakheeeir. Khabari nist azin pesar. Baz maloom nist ba kodoom doostesh biroone. Ah!";
const isHomeMessage = "Baaaaleeee, shaazdeh pesar dar khoonast o gooshio bar nemidare. Be maman babash rafte.";

exports.lookup = function (macAddress, req) {
    exec("hcitool name " + macAddress, function (err, stdout, stderr) {
        let response = stdout;
        if (err) {
            response = err;
            console.log("err: " + err);
        }
        console.log("stdout: " + stdout);
        respondSlack.updateClient(response ? isHomeMessage : missingMessage, req)
    });
}