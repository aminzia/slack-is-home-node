const request = require('request');

exports.updateClient = function (body, req) {
    let clientServerOptions = {
        uri: req.body["response_url"],
        body: JSON.stringify({
            'text': body,
            'replace_original': true
        }),
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        }
    };
    request(clientServerOptions, function (error, response) {
        console.log(error, response.body);
    });
};
