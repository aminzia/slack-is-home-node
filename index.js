const express = require('express');
const phoneLookup = require('./phone_lookup');
const bodyParser = require('body-parser');
const textToSpeech = require('./ttspeech');
const respondSlack = require('./respond-slack');
const app = express();
const port = 8080;
let macAddress = "7C:D9:5C:B7:4D:DE";

app.use(bodyParser.urlencoded({ extended: true }));

app.post('/say-something', function (req, res) {
    console.log(req.body);
    res.send("Dare boland mige alan...");
    textToSpeech.speak(req.body["text"]);
    respondSlack.updateClient("Harfesh tamoom shod.",req);
});

app.post('/amin-is-home', function (req, res) {
    console.log(req.body);
    res.send("Loading...");
    phoneLookup.lookup(macAddress, req);

});

app.get('/health', function (req, res) {
    console.log("Got the health check");
    res.send(`Server is listening to port: ${port}`);
});

app.post('/mac-address', function (req, res) {
    let newMacAddress = req.body["text"];
    console.log(req.body);
    console.log("Mac Address passed: " + newMacAddress);
    if (!newMacAddress) {
        res.send("current: " + macAddress);
    } else {
        macAddress = newMacAddress;
        res.send("updated to: " + macAddress);
    }
});

app.listen(port, () => console.log(`Listening on port ${port}!`));