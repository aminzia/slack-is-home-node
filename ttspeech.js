// automatically pick platform
const say = require('say');
//I'm getting this on raspberryPi: aplay: main:722: audio open error: No such file or directory
exports.speak = function (text) {
// Fire a callback once the text has completed being spoken
    say.speak(text, 'voice_kal_diphone', 1.0, (err) => {
        if (err) {
            return console.error(err)
        }
        console.log('Text has been spoken.')
    });
}